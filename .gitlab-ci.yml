# This CI file has the intent of:
# Have 2 pipelines: Merges and Releases
# Check if we've got the version number
# Every commit is linted
# Every Commit & Merge Request is  linted and tested against the resulting repo
# Every Commit & Merge Request updates the LATEST BUILD
# TODO -- Every Merge Request increases the Patch level
# Every Tagged commit with v* tag creates a new release if the previous steps have been successful with a new major or minor level.
# See https://gitlab.com/guided-explorations/cfg-data/write-ci-cd-variables-in-pipeline/-/blob/master/.gitlab-ci.yml
# See for writing to a file https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/npm.gitlab-ci.yml
# See for github help with foundry and packaging: https://github.com/janssen-io/foundry-github-workflow#alternative-versioning

stages:
  - test
  - build
  - upload
  - release

###########
# VARIABLES
###########
variables:
  DOCKER_DRIVER: overlay2
  IMAGE_VERSION: ${CI_COMMIT_REF}.${CI_PIPELINE_ID}
  IMAGE_NAME: 'CONAN-2D20'
  FVTT_SYSTEM_NAME: $(node -p "require('./system.json').name")
  FVTT_SYSTEM_VERSION: $(node -p "require('./system.json').version")
  # /^v(\d+)\.(\d+)\.(\d+)$/
  RELEASE_VERSION_NEW_MAJOR: /^v(\d+)\.\d+\.\d+$/ && $CI_COMMIT_TAG
  RELEASE_VERSION_NEW_MINOR: /^v\d+\.(\d+)\.\d+$/ && $CI_COMMIT_TAG
  RELEASE_VERSION_NEW_PATCH: /^v\d+\.\d+\.(\d+)$/ && $CI_COMMIT_TAG
  VERSIONCOMPONENTTOINCREMENT: '-p'
  # -M = Major (first position), -m = minor (second position), -p = patch (third position)

###########
# GLOBAL PIPELINE  # TRIGGER: COMMITS TO NON DEFAULT BRANCH && A MERGE REQUEST
# Commented out for https://gitlab.com/fvtt-modiphius/foundryvtt-conan2d20/-/issues/221
###########
# dco:
#  image: ubuntu
#  stage: test
#  only:
#    - branches
#  except:
#    - tags
#    - master
#  before_script:
#    - apt update -y
#    - apt install -y python3 git wget
#  script:
#    - wget https://raw.githubusercontent.com/christophebedard/dco-check/master/dco_check/dco_check.py
#    - python3 dco_check.py
  # optional

lint:
  image: node:16
  stage: test
  only:
    - branches
  except:
    - tags
    - master
  before_script:
    # npm stores cache data in the home folder ~/.npm but you can’t cache things outside of the project directory (.npm/).
    # - npm ci --cache .npm --prefer-offline -- Removed on 2021-04-18 due to major slow down
    - '## Installing eslint and plugins ##'
    - npm install
    - '## Installing pre-commit ##'
    - curl https://pre-commit.com/install-local.py | python -
  script:
    # Run Precommit Checks
    - echo "#### Executing Precommit Checks."
    - /root/bin/pre-commit run --all-files

build:
  image: node:16
  stage: build
  before_script:
    # npm stores cache data in the home folder ~/.npm but you can’t cache things outside of the project directory (.npm/).
    # - npm ci --cache .npm --prefer-offline
    - echo "preliminary script -- Updating APT-Get & Installing zip and webpack"
    - apt-get update
    - apt-get install zip
    - npm i webpack
  script:
    - echo "build execution -- Building with webpack."
    - npm run build
    - mv dist conan2d20
    - ls -la conan2d20
    - echo "build execution -- Zipping files."
    - zip -9 -u -r -q conan2d20.zip conan2d20
  ###########
  # ARTIFACTS  # Items to keep beyond the pipeline execution
  ###########
  artifacts:
    name: conan2d20
    expire_in: never
    when: on_success
    paths:
      - conan2d20.zip
      - system.json

#################################
# RELEASE PIPELINE  # TRIGGER: ON TAGGED COMMIT IN A MERGE REQUEST
#################################
upload:
  image: curlimages/curl:latest
  stage: upload
  only:
    - tags
    - /^(^v[0-9]+(?:.[0-9]+)+$).*$/
  script:
    - export PKG_VERSION="$(echo ${CI_COMMIT_TAG}|cut -c 2-)"
    - echo "Preparing Release -- tagging package with version."
    - cp conan2d20.zip conan2d20-${CI_COMMIT_TAG}.zip
    - echo "Preparing Release -- Uploading Package"
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file conan2d20-${CI_COMMIT_TAG}.zip "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/conan2d20/${PKG_VERSION}/conan2d20-${CI_COMMIT_TAG}.zip"'
    - echo "Preparing Release -- uploading system manifest"
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file system.json "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/conan2d20/${PKG_VERSION}/system.json"'
  artifacts:
    name: conan2d20-${CI_COMMIT_TAG}
    expire_in: never
    when: on_success
    paths:
      - conan2d20-${CI_COMMIT_TAG}.zip

release:
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  stage: release
  only:
    - tags
    - /^(^v[0-9]+(?:.[0-9]+)+$).*$/
  script:
    - export PKG_VERSION="$(echo ${CI_COMMIT_TAG}|cut -c 2-)"
    - echo 'executing release -- creating release'
    - release-cli create --name "Release ${CI_COMMIT_TAG}" --description "CHANGELOG.md" --tag-name $CI_COMMIT_TAG --assets-link "{\"name\":\"conan2d20-${CI_COMMIT_TAG}.zip\",\"url\":\"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/conan2d20/${PKG_VERSION}/conan2d20-${CI_COMMIT_TAG}.zip\"}" --assets-link "{\"name\":\"system.json\",\"url\":\"${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/conan2d20/${PKG_VERSION}/system.json\"}"
