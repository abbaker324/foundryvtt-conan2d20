// This macro can be used to perform the following housekeeping
// tasks when you want to start a new scene:
//
//  - Reset every player character's Vigor and Resolve to their
//    current maximum.
//  - Clears any left over personal momentum for every player
//  - character
//  - Reduces the players' shared Momentum pool by one
//
// NOTE: Only users with the Game Master user role can run this macro
//
game.conan2d20.macros.newScene();
