/* eslint-disable no-unused-vars */

// import {CONFIG} from '../../scripts/config';
import TraitSelector from '../system/trait-selector';

export default class ItemSheetConan2d20 extends ItemSheet {
  static get defaultOptions() {
    const options = super.defaultOptions;
    mergeObject(options, {
      classes: options.classes.concat(['conan2d20', 'item', 'sheet']),
      width: 760,
      height: 500,
      template: 'systems/conan2d20/templates/items/item-sheet.html',
      resizable: false,
      tabs: [
        {
          navSelector: '.sheet-tabs',
          contentSelector: '.sheet-body',
          initial: 'description',
        },
      ],
    });
    return options;
  }

  /**
   * Override header buttons to add custom ones.
   */
  _getHeaderButtons() {
    let buttons = super._getHeaderButtons();

    // Add "Post to chat" button
    buttons = [
      {
        label: 'Post',
        class: 'post',
        icon: 'fas fa-comment',
        onclick: ev => this.item.postItem(ev),
      },
    ].concat(buttons);

    return buttons;
  }

  /* -------------------------------------------- */

  /**
   * Prepare item sheet data
   * Start with the base item data and extending with additional properties for rendering.
   */
  getData() {
    const data = super.getData();

    data.attributes = CONFIG.CONAN.attributes;

    const {type} = this.item;
    mergeObject(data, {
      type,
      hasSidebar: true,
      sidebarTemplate: () =>
        `systems/conan2d20/templates/items/${type}-sidebar.html`,
      hasDetails: [
        'weapon',
        'armor',
        'talent',
        'kit',
        'action',
        'display',
        'enchantment',
        'npcattack',
        'npcaction',
        'transportation',
      ].includes(type),
      detailsTemplate: () =>
        `systems/conan2d20/templates/items/${type}-details.html`,
    });

    data.availability = CONFIG.CONAN.availabilityTypes;

    if (type === 'armor') {
      data.armorQualities = CONFIG.CONAN.armorQualities;
      data.armorTypes = CONFIG.CONAN.armorTypes;
      data.coverageTypes = CONFIG.CONAN.coverageTypes;
    } else if (type === 'weapon') {
      data.damageDice = CONFIG.CONAN.damageDice;
      data.weaponDamage = CONFIG.CONAN.weaponDamage;
      data.weaponGroups = CONFIG.CONAN.weaponGroups;
      data.weaponQualities = CONFIG.CONAN.weaponQualities;
      data.weaponRanges = CONFIG.CONAN.weaponRanges;
      data.weaponReaches = CONFIG.CONAN.weaponReaches;
      data.weaponSizes = CONFIG.CONAN.weaponSizes;
      data.weaponTypes = CONFIG.CONAN.weaponTypes;

      const sortedSkills = [];
      for (let skill in CONFIG.CONAN.skills) {
        if (skill === 'oth') continue; // not sure what this is, ignore?

        sortedSkills.push({
          key: skill,
          name: CONFIG.CONAN.skills[skill],
        });
      }

      sortedSkills.sort((a, b) => {
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      });

      data.overrideSkills = sortedSkills;

      this._prepareQualities(CONFIG.CONAN.weaponQualities);
    } else if (type === 'npcattack') {
      data.attackTypes = CONFIG.CONAN.npcAttackTypes;
      data.damageDice = CONFIG.CONAN.damageDice;
      data.damageTypes = CONFIG.CONAN.damageTypes;
      data.hasSidebar = false;
      data.weaponDamage = CONFIG.CONAN.weaponDamage;
      data.weaponQualities = CONFIG.CONAN.weaponQualities;
      data.weaponRanges = CONFIG.CONAN.weaponRanges;
      data.weaponReaches = CONFIG.CONAN.weaponReaches;

      this._prepareQualities(CONFIG.CONAN.weaponQualities);
    } else if (type === 'talent') {
      data.categories = CONFIG.CONAN.actionCategories;
      data.talentActionTypes = CONFIG.CONAN.actionTypes;
      data.talentSkills = CONFIG.CONAN.skills;
      data.talentTypes = CONFIG.CONAN.talentTypes;
    } else if (type === 'transportation') {
      data.animals = CONFIG.CONAN.transpoAnimals;
      data.boatType = CONFIG.CONAN.transpoBoatTypes;
      data.capabilities = CONFIG.CONAN.transpoCapabilities;
      data.cartType = CONFIG.CONAN.transpoCartTypes;
      data.categories = CONFIG.CONAN.transpoCategories;
      data.mountType = CONFIG.CONAN.transpoMountTypes;
    } else if (type === 'display') {
      data.displayQualities = CONFIG.CONAN.weaponQualities;
      data.displayRanges = CONFIG.CONAN.weaponRanges;
      data.displaySkills = CONFIG.CONAN.skills;

      const displayDice = mergeObject(
        CONFIG.CONAN.damageDice,
        CONFIG.CONAN.displayDamageDice
      );

      data.damageDice = displayDice;

      this._prepareQualities(CONFIG.CONAN.weaponQualities);
    } else if (type === 'action') {
      const actorWeapons = [];
      if (this.actor) {
        for (const i of this.actor.items) {
          if (i.type === 'weapon') actorWeapons.push(i);
        }
      }

      // TODO add function to get action img
      // const actionType = data.data.actionType.value || 'action';
      // data.item.img = this._getActionImg(actionType);
      data.actionCategories = CONFIG.CONAN.actionCategories;
      data.actionCounts = CONFIG.CONAN.actionCounts;
      data.actionTypes = CONFIG.CONAN.actionTypes;
      data.weapons = actorWeapons;
      // TODO generate action tags
      // data.actionTags = [data.data.qualities.value].filter(t => !!t);
    } else if (type === 'enchantment') {
      data.blindingStrengths = CONFIG.CONAN.enchantmentBlindingStrengths;
      data.coverageTypes = CONFIG.CONAN.coverageTypes;
      data.damageDice = CONFIG.CONAN.damageDice;
      data.difficulty = CONFIG.CONAN.availabilityTypes;
      data.enchantmentEffects = CONFIG.CONAN.weaponQualities;
      data.enchantmentStrengths = CONFIG.CONAN.enchantmentStrengths;
      data.enchantmentTypes = CONFIG.CONAN.enchantmentTypes;
      data.explodingItems = CONFIG.CONAN.enchantmentExplodingItems;
      data.hasSidebar = false;
      data.ingredient = CONFIG.CONAN.enchantmentIngredients;
      data.lotusPollenColors = CONFIG.CONAN.lotusPollenColors;
      data.lotusPollenForms = CONFIG.CONAN.lotusPollenForms;
      data.lotusPollenUses = CONFIG.CONAN.lotusPollenUses;
      data.talismanTypes = CONFIG.CONAN.enchantmentTalismanTypes;
      data.upasGlassSizes = CONFIG.CONAN.upasGlassSizes;
      data.volatilities = CONFIG.CONAN.enchantmentVolatilities;
    } else if (type === 'spell') {
      data.difficulty = CONFIG.CONAN.availabilityTypes;
      data.hasSidebar = false;
    } else if (type === 'miscellaneous') {
      data.hasSidebar = true;
    } else if (type === 'npcaction') {
      data.actionTypes = CONFIG.CONAN.npcActionTypes;
      data.hasSidebar = false;
    } else if (type === 'kit') {
      data.kitSkills = CONFIG.CONAN.skills;
      data.kitTypes = CONFIG.CONAN.kitTypes;
      data.uses = CONFIG.CONAN.kitUses;
    }

    return data;
  }

  onTraitSelector(event) {
    event.preventDefault();
    const a = $(event.currentTarget);
    const options = {
      name: a.parents('label').attr('for'),
      title: a.parent().text().trim(),
      choices: CONFIG.CONAN[a.attr('data-options')],
      hasValues: a.attr('data-has-values') === 'true',
      allowEmptyValues: a.attr('data-allow-empty-values') === 'true',
    };
    new TraitSelector(this.item, options).render(true);
  }

  activateListeners(html) {
    super.activateListeners(html);

    // save checkbox changes
    html.find('input[type="checkbox"]').change(event => this._onSubmit(event));

    // activate trait selector
    html.find('.trait-selector').click(ev => this.onTraitSelector(ev));

    // add row to spell momentum spends
    html.find('.spend-row-add').click(ev => this.insertSpendRow(ev));

    // add row to spell alternate effects
    html.find('.alt-row-add').click(ev => this.insertAltRow(ev));

    // delete row from spell alternate effects
    html.find('.alt-row-delete').click(ev => this.deleteAltRow(ev));

    // delete row from spell momentum spends
    html.find('.spend-row-delete').click(ev => this.deleteSpendRow(ev));
  }

  _prepareQualities(traits) {
    if (traits === undefined) return;

    for (const [t, choices] of Object.entries(traits)) {
      const trait = traits[t] || {value: [], selected: []};

      if (Array.isArray(trait)) {
        trait.selected = {};
        for (const entry of trait) {
          if (typeof entry === 'object') {
            let text = `${choices[entry.type]}`;
            if (entry.value !== '') text = `${text} (${entry.value})`;
            trait.selected[entry.type] = text;
          } else {
            trait.selected[entry] = choices[entry] || `${entry}`;
          }
        }
      } else if (trait.value) {
        trait.selected = trait.value.reduce((obj, b) => {
          obj[b] = choices[b];
          return obj;
        }, {});
      }

      if (trait.custom) trait.selected.custom = trait.custom;
    }
  }

  _onChangeInput(event) {
    return this._onSubmit(event);
  }

  insertSpendRow(_event) {
    try {
      const table = document.getElementById('spellSpends');
      const itemId = this.item._id;
      const index = table.rows.length - 1;
      const key = `system.effects.momentum.${[index + 1]}`;
      this.item.update({
        id: itemId,
        [key]: {type: '', difficulty: '', effect: ''},
      });
    } catch (e) {
      alert(e);
    }
  }

  insertAltRow(_event) {
    try {
      const table = document.getElementById('altEffects');
      const itemId = this.item._id;
      const index = table.rows.length - 1;
      const key = `system.effects.alternative.${[index + 1]}`;
      this.item.update({
        id: itemId,
        [key]: {type: '', difficulty: '', effect: ''},
      });
    } catch (e) {
      alert(e);
    }
  }

  deleteAltRow(_event) {
    try {
      const table = document.getElementById('altEffects');
      const toDelete = table.rows.length - 1;
      const key = `system.effects.alternative.-=${[toDelete]}`;
      this.item.update({[key]: null});
    } catch (e) {
      alert(e);
    }
  }

  deleteSpendRow(_event) {
    try {
      const table = document.getElementById('spellSpends');
      const toDelete = table.rows.length - 1;
      const key = `system.effects.momentum.-=${[toDelete]}`;
      this.item.update({[key]: null});
    } catch (e) {
      alert(e);
    }
  }
}
