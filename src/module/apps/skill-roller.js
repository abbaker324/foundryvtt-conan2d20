export default class SkillRoller extends Application {
  constructor(object, options) {
    super(object, options);

    object = {
      // actorId: 'SLpeINPb0IhqFmHz', // PC
      actorId: 'iasORxrslZl9iooP', // NPC
      attribute: 'int',
      expertise: 'knw',
      skill: 'res',
    }; //test data

    this.actor = null;

    if (object?.actorId) {
      this.actor = game.actors.get(object.actorId);

      // Die if we can't get the actor

      this.attribute = object.attribute || '';
      this.expertise = object.expertise || '';
      this.skill = object.skill || '';
    }

    this.isActorRoll = this.actor ? true : false;

    this.isNpc = false;
    if (this.isActorRoll) {
      this.isNpc = this.actor.type === 'npc';
    }

    this.difficulties = [
      {
        active: false,
        tooltip: game.i18n.localize('CONAN.skillRollDifficultyLevels.0'),
      },
      {
        active: true,
        tooltip: game.i18n.localize('CONAN.skillRollDifficultyLevels.1'),
      },
      {
        active: false,
        tooltip: game.i18n.localize('CONAN.skillRollDifficultyLevels.2'),
      },
      {
        active: false,
        tooltip: game.i18n.localize('CONAN.skillRollDifficultyLevels.3'),
      },
      {
        active: false,
        tooltip: game.i18n.localize('CONAN.skillRollDifficultyLevels.4'),
      },
      {
        active: false,
        tooltip: game.i18n.localize('CONAN.skillRollDifficultyLevels.5'),
      },
    ];

    this.dice = [
      {active: true},
      {active: true},
      {active: false},
      {active: false},
      {active: false},
    ];

    // default data
    this.rollData = {
      bonusDice: 0,
      bonusMomentum: 0,
      bonusSuccesses: 0,
      difficulty: 1,
      focus: 0,
      numDice: CONFIG.BASE_2D20_DICE,
      numDoom: 0,
      numFortune: 0,
      numMomentum: 0,
      tn: 7,
    };
  }

  static get defaultOptions() {
    return foundry.utils.mergeObject(super.defaultOptions, {
      id: 'skill-roller',
      classes: ['conan2d20', 'skill-roller'],
      template: 'systems/conan2d20/templates/apps/skill-roller.html',
      width: 320,
      // width: 'auto',
      height: 'auto',
      submitOnChange: false,
    });
  }

  get title() {
    return `${game.i18n.localize('CONAN.skillRollerTitle')}`;
  }

  activateListeners(html) {
    super.activateListeners(html);
    const me = this;

    // Difficulty setting buttons
    html
      .find('.difficulty')
      .on('click', this._onClickDifficultyButton.bind(this));

    // Dice icons
    html.find('.dice').on('click', this._onClickDiceIcon.bind(this));

    // Quantity buttons
    html.find('.quantity').each(function () {
      const spinner = $(this);
      const input = spinner.find('input[type="number"]');
      const btnUp = spinner.find('.quantity-up');
      const btnDown = spinner.find('.quantity-down');
      const type = input.attr('data-quantity-type');

      console.log(`quantity type: ${type}`);

      btnUp.click(function () {
        me[`_${type}Inc`](input);
      });

      btnDown.click(function () {
        me[`_${type}Dec`](input);
      });
    });
  }

  async getData() {
    const data = {
      actorData: duplicate(this.actor),
      attributes: this._sortedAttributes(),
      dice: this.dice,
      difficulties: this.difficulties,
      expertiseFields: this._sortedExpertiseFields(),
      isActorRoll: this.isActorRoll,
      isNpc: this.isNpc,
      rollData: this.rollData,
      selectedAttribute: this.attribute,
      selectedExpertise: this.expertise,
      selectedSkill: this.skill,
      skills: this._sortedSkills(),
    };

    if (this.isActorRoll) {
      data.testDetails = this._getTestDetails();
    }

    return data;
  }

  async _adjustBoughtDice(numDice) {
    // We special case things if a single dice is selected, as this is an
    // assist roll
    //
    if (numDice === CONFIG.ASSIST_2D20_DICE) {
      // Assist roll.  No additional dice can be purchased for those, and no
      // bonuses apply
      //
      this.rollData.bonusDice = 0;
      this.rollData.bonusMomentum = 0;
      this.rollData.bonusSuccesses = 0;
      this.rollData.numDoom = 0;
      this.rollData.numFortune = 0;
      this.rollData.numMomentum = 0;

      this.rollData.numDice = numDice;

      await this._updateAllFormValues();

      return true;
    }

    // fixedDice is the base 2d20 dice, plus any fortune and bonus d20s
    // already entered.
    //
    const fixedDice =
      CONFIG.BASE_2D20_DICE +
      this.rollData.bonusDice +
      this.rollData.numFortune;

    // If the requested amount of dice is below this then we can't adjust the
    // dice until either the number of bonus d20s and/or number of fortune
    // spent has been reduced.
    //
    if (numDice < fixedDice) return false;

    let availableToSpend = 0;
    if (this.isNpc) {
      availableToSpend = await this.actor.getAvailableDoom();
    } else {
      availableToSpend = await this.actor.getAvailableMomentum();
    }

    let diceToAllocate = numDice - fixedDice;

    let newDoom = 0;
    let newMomentum = 0;

    while (diceToAllocate > 0) {
      if (!this.isNpc && availableToSpend > 0) {
        availableToSpend--;
        newMomentum++;
      } else {
        newDoom++;
      }
      diceToAllocate--;
    }

    this.rollData.numDice = numDice;

    this.rollData.numDoom = newDoom;
    this.rollData.numMomentum = newMomentum;

    this._updateAllFormValues();

    return true;
  }

  async _bonusDiceDec(input) {
    console.log(`_bonusDiceDec: ${input.val()}`);
  }

  async _bonusDiceInc(input) {
    console.log(`_bonusDice: ${input.val()}`);
  }

  async _bonusMomentumDec(input) {
    let currentValue = parseInt(input.val());

    if (currentValue === 0) return;

    this.rollData.bonusMomentum--;
    this._updateAllFormValues();
  }

  async _bonusMomentumInc() {
    if (this.rollData.bonusMomentum < 9) this.rollData.bonusMomentum++;
    this._updateAllFormValues();
  }

  async _bonusSuccessesDec(input) {
    let currentValue = parseInt(input.val());

    if (currentValue === 0) return;

    this.rollData.bonusSuccesses--;
    this._updateAllFormValues();
  }

  async _bonusSuccessesInc() {
    if (this.rollData.bonusSuccesses < 9) this.rollData.bonusSuccesses++;
    this._updateAllFormValues();
  }

  _getTestDetails() {
    const difficulty = game.i18n.localize(
      `CONAN.skillRollDifficultyLevels.${this.rollData.difficulty}`
    );

    const actorSkills = this.actor.system.skills;

    let tn = parseInt(this.actor.system.attributes[this.attribute].value) || 0;
    if (this.isNpc) {
      tn += parseInt(actorSkills[this.expertise].value) || 0;
    } else {
      tn += parseInt(actorSkills[this.skill].expertise.value) || 0;
    }

    let skillFocus = 0;
    if (this.isNpc) {
      skillFocus = parseInt(actorSkills[this.expertise].value) || 0;
    } else {
      skillFocus = parseInt(actorSkills[this.skill].focus.value) || 0;
    }

    return `${difficulty}, TN ${tn}, Focus ${skillFocus}`;
  }

  async _numDoomDec(input) {
    let currentValue = parseInt(input.val());

    if (currentValue === 0) return;

    this.rollData.numDoom--;
    this.rollData.numDice--;

    input.val(this.rollData.numDoom);
    await this._adjustBoughtDice(this.rollData.numDice);
    await this._updateDiceIcons();
  }

  async _numDoomInc(input) {
    let currentValue = parseInt(input.val());
    let numAvailableDoom = await this.actor.getAvailableDoom();

    let doomAvailable = true; // default for non-NPCs
    if (this.isNpc) {
      // Fortune used by NPCs costs 3 Doom per Fortune
      numAvailableDoom -= this.rollData.numFortune * 3;
      doomAvailable = currentValue < numAvailableDoom;
    }

    const currentNumFixedDice =
      CONFIG.BASE_2D20_DICE +
      this.rollData.numFortune +
      this.rollData.bonusDice;

    if (doomAvailable && currentNumFixedDice < CONFIG.MAX_2D20_DICE) {
      this.rollData.numDoom++;

      input.val(this.rollData.numDoom);
      this._updateDiceIcons();

      if (this.rollData.numDice < 5) {
        this.rollData.numDice++;
      }

      await this._adjustBoughtDice(this.rollData.numDice);
      await this._updateDiceIcons();
    }
  }

  async _numFortuneDec(input) {
    let currentValue = parseInt(input.val());

    if (currentValue === 0) return;

    this.rollData.numFortune--;
    this.rollData.numDice--;

    input.val(this.rollData.numFortune);
    await this._adjustBoughtDice(this.rollData.numDice);
    await this._updateDiceIcons();
  }

  async _numFortuneInc(input) {
    let currentValue = parseInt(input.val());
    const availableFortune = await this.actor.getAvailableFortune();

    const currentNumFixedDice =
      CONFIG.BASE_2D20_DICE +
      this.rollData.numFortune +
      this.rollData.bonusDice;

    if (
      currentValue < availableFortune &&
      currentNumFixedDice < CONFIG.MAX_2D20_DICE
    ) {
      this.rollData.numFortune++;

      input.val(this.rollData.numFortune);
      this._updateDiceIcons();

      if (this.rollData.numDice < 5) {
        this.rollData.numDice++;
      }

      await this._adjustBoughtDice(this.rollData.numDice);
      await this._updateDiceIcons();
    }
  }

  async _numMomentumDec(input) {
    console.log(`_numMomentumDec: ${input.val()}`);
  }

  async _numMomentumInc(input) {
    console.log(`_incNumMomentum: ${input.val()}`);
  }

  async _onClickDiceIcon(event) {
    const diceIcon = $(event.currentTarget);

    const numDice = parseInt(diceIcon.attr('data-dice-number')) + 1;
    const prevNumDice = this.rollData.numDice;

    if (numDice === prevNumDice) return; // Nothing has changed

    // We only need to adjust the bought dice values if this is an Actor based
    // check.
    //
    // For the simple skill checks we do not adjust any momentum/doom/fortune,
    // and just roll the number of selected dice.
    //
    let diceAdjusted = true;
    if (this.isActorRoll) {
      // TODO: NPCs can exceed available doom when clicking on dice to set
      diceAdjusted = await this._adjustBoughtDice(numDice, prevNumDice);
    } else {
      this.rollData.numDice = numDice;
    }

    if (diceAdjusted) {
      // Hide dice purchasing and bonus sections if there is only one dice
      // selected, as this will be an assistance roll which can't use those
      // bonuses.
      //
      if (numDice === 1) {
        $('.extra-dice-hideable').hide();
      } else {
        $('.extra-dice-hideable').show();
      }
      await this._updateDiceIcons();
    }
  }

  async _onClickDifficultyButton(event) {
    event.preventDefault();

    const button = $(event.currentTarget);

    const difficulty = parseInt(button.attr('data-difficulty'));

    if (difficulty === this.rollData.difficulty) return;

    this.rollData.difficulty = difficulty;

    button.siblings().removeClass('active');
    button.addClass('active');

    this._updateTestDetails();
  }

  async _updateAllFormValues() {
    const me = this;

    $('.quantity').each(function () {
      const spinner = $(this);
      const input = spinner.find('input[type="number"]');
      const type = input.attr('data-quantity-type');
      input.val(me.rollData[type]);
    });
  }

  _sortedAttributes() {
    return this._sortObjectsByName(CONFIG.CONAN.attributes);
  }

  _sortedExpertiseFields() {
    return this._sortObjectsByName(CONFIG.CONAN.expertiseFields);
  }

  _sortedSkills() {
    return this._sortObjectsByName(CONFIG.CONAN.skills);
  }

  _sortObjectsByName(object) {
    const sortedData = [];
    for (let item in object) {
      sortedData.push({
        key: item,
        name: object[item],
      });
    }

    sortedData.sort((a, b) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    });

    return sortedData;
  }

  async _updateDiceIcons() {
    const numDice = this.rollData.numDice;
    const me = this;

    $('.dice').each(function () {
      const icon = $(this);
      const iconNum = parseInt(icon.attr('data-dice-number'));

      icon.removeClass('fortune selected unselected');

      if (iconNum < me.rollData.numFortune) {
        icon.addClass('fortune');
        icon.html('1');
      } else if (iconNum < numDice) {
        icon.addClass('selected');
        icon.html('?');
      } else {
        icon.addClass('unselected');
        icon.html('&nbsp;');
      }
    });
  }

  async _updateTestDetails() {
    const text = this._getTestDetails();
    $('.test-details').html(text);
  }
}
