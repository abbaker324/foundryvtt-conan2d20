/* eslint-disable no-shadow */

import ConanChat from './chat';
import {CONFIG} from '../../scripts/config';
import Conan2d20Actor from '../actor/actor';
import MomentumBanker from '../apps/momentum-banker';

export default class Conan2d20Dice {
  /**
   * Reference:
   * @param {number} diceQty          Number of d20 to roll
   * @param {number} tn               The Skill + Attribute to roll under
   * @param {number} focus            The Focus value to roll under for extra successes
   * @param {number} autoSuccess      How many successes to add
   * @param {boolean} trained         Is the skill trained, adds 19 to generate complications if not
   * @param {number} difficulty       Difficulty level of the skill check
   * @param {any} cardData            Data for rendering a chat message on completed roll
   */
  static async calculateSkillRoll(
    diceQty,
    tn,
    focus = 0,
    trained = false,
    difficulty = 0,
    autoSuccess,
    cardData,
    fixedrolls,
    bonusMomentum = 0
  ) {
    const rollErr = {
      diceQty:
        'Conan 2D20 | Error in Skill Check, D20 Quantity is not a Number.',
      tn: 'Conan 2D20 | Error in Skill Check, Target Number is not a Number.',
      focus: 'Conan 2D20 | Error in Skill Check, Focus is not a Number.',
      auto:
        'Conan 2D20 | Error in Skill Check, Automatic Successes is not a Number.',
      train: 'Conan 2D20 | Error in Skill Check, Trained is not a Boolean.',
    };
    if (Number.isNaN(diceQty)) {
      throw rollErr.diceQty;
    }
    if (Number.isNaN(tn)) {
      throw rollErr.tn;
    }
    if (Number.isNaN(focus)) {
      throw rollErr.focus;
    }
    if (Number.isNaN(autoSuccess)) {
      throw rollErr.auto;
    }
    if (typeof trained !== 'boolean') {
      throw rollErr.train;
    }

    let rollResult = {};
    let successes = 0;
    let crits = 0;
    let momentumGenerated = 0;
    let complications = 0;
    let rolls = [];
    let rollInstance;

    if (diceQty === 0) {
      rolls = [];
    } else {
      rollInstance = new Roll(`${diceQty}d20cs<=${tn}`);
      rolls = await rollInstance.roll({async: true});
      rolls = rolls.terms[0].results;
      if (game.dice3d) {
        await Conan2d20Dice.showDiceSoNice(
          rollInstance,
          game.settings.get('core', 'rollMode')
        );
      }
    }

    // Handle fortune point spends which give an autosuccess with the dice
    // forced to be a "1"
    //
    let i;
    if (autoSuccess !== undefined && autoSuccess > 0) {
      for (i = 0; i < autoSuccess; i += 1) {
        const successRoll = {result: 1};
        rolls.unshift(successRoll);
      }
    }

    // If we're performing a reroll, then merge the rerolled dice into the
    // correct slots in the original results which makes it more obvious how
    // the new roll performed rather than shifting the position of all results
    //
    let reroll = fixedrolls && fixedrolls.length > 0 ? true : false;

    if (reroll) {
      rolls = await this._mergeRerolls(rolls, fixedrolls);
    }

    // Go through all rolls and index their placement for future use.
    // Also flag criticals, complications and successes so they can be
    // identified for display purposes.
    //
    rolls.forEach((roll, i) => {
      roll.index = i;
      roll.complication = false;
      roll.critical = false;
      if (!trained) {
        if (roll.result >= 19) {
          roll.complication = true;
          complications += 1;
        }
      } else if (roll.result === 20) {
        roll.complication = true;
        complications += 1;
      }
      if (roll.result <= focus) {
        roll.critical = true;
        roll.success = false;
        crits += 1;
      } else if (roll.result <= tn) {
        successes += 1;
      }
    });

    successes += crits * 2;

    let result = 'failure';
    if (successes >= difficulty) {
      momentumGenerated = successes - difficulty;
      if (bonusMomentum > 0) {
        momentumGenerated += bonusMomentum;
      }
      result = 'success';
    }

    const rollData = {
      tn,
      focus: focus || 0,
      trained,
      reroll,
    };

    rollResult = {
      difficulty,
      successes,
      complications,
      momentumGenerated,
      result,
      rolls,
      bonusMomentum,
    };

    const actor = game.actors.get(cardData.speaker.actor);

    const updateActorData = {};
    updateActorData['system.momentum'] = momentumGenerated;
    actor.update(updateActorData);

    ConanChat.renderRollCard(rollResult, rollData, cardData, 'skill');
  }

  static async calculateSoakRoll(diceQty = 1, cardData, rollData, fixedrolls) {
    const soakRollInstance = new Roll(`${diceQty}dp`);
    let soakRolls = await soakRollInstance.roll({async: true});

    if (game.dice3d) {
      await Conan2d20Dice.showDiceSoNice(
        soakRollInstance,
        game.settings.get('core', 'rollMode')
      );
    }

    let rolls = soakRolls.terms[0].results;

    // Populate each roll with its display value and a null index which will
    // be set correctly after any reroll merges have occured.
    //
    rolls.forEach((roll, index) => {
      roll.index = null;
      roll.display = soakRolls.terms[0].resultValues[index];
    });

    // If we're performing a reroll, then merge the rerolled dice into the
    // correct slots in the original results which makes it more obvious how
    // the new roll performed rather than shifting the position of all results
    //
    let reroll = fixedrolls && fixedrolls.length > 0 ? true : false;

    if (reroll) {
      rolls = await this._mergeRerolls(rolls, fixedrolls);
    }

    let effects = 0;
    let soak = 0;

    // Go through all rolls and index their placement for future use.
    //
    rolls.forEach((roll, index) => {
      roll.index = index;
      effects += roll.effect ? 1 : 0;
      soak += roll.result <= 2 ? roll.result : 0;
    });

    soak += effects;

    rollData.reroll = reroll;

    const rollResult = {
      soak,
      effects,
      rolls,
    };

    ConanChat.renderRollCard(rollResult, rollData, cardData, 'soak');
  }

  static async calculateDamageRoll(
    diceQty = 1,
    damageType,
    cardData,
    rollData,
    fixedrolls,
    prevHitLocation
  ) {
    const damageRollInstance = new Roll(`${diceQty}dp`);
    let damageRolls = await damageRollInstance.roll({async: true});

    if (game.dice3d) {
      await Conan2d20Dice.showDiceSoNice(
        damageRollInstance,
        game.settings.get('core', 'rollMode')
      );
    }

    let rolls = damageRolls.terms[0].results;

    // Populate each roll with its display value and a null index which will
    // be set correctly after any reroll merges have occured.
    //
    rolls.forEach((roll, index) => {
      roll.index = null;
      roll.display = damageRolls.terms[0].resultValues[index];
    });

    // If we're performing a reroll, then merge the rerolled dice into the
    // correct slots in the original results which makes it more obvious how
    // the new roll performed rather than shifting the position of all results
    //
    let reroll = fixedrolls && fixedrolls.length > 0 ? true : false;

    if (reroll) {
      rolls = await this._mergeRerolls(rolls, fixedrolls);
    }

    let effects = 0;
    let damage = 0;

    // Go through all rolls and index their placement for future use.
    //
    rolls.forEach((roll, index) => {
      roll.index = index;
      effects += roll.effect ? 1 : 0;
      damage += roll.result <= 2 ? roll.result : 0;
    });

    // Add momentum spend damage at a 1-for-1 level
    damage += rollData.momentumModifier;

    // Only add additional effect damage if not an improvised attack
    if (!rollData.improvised) {
      damage += effects;
    }

    // Get a hit location if this isn't a reroll, otherwise used the previous
    // hit location value
    //
    let hitLocation = prevHitLocation;
    if (!hitLocation && rollData.damageType !== 'mental') {
      let locationRoll = await new Roll('1d20').roll({async: true});
      const location = parseInt(locationRoll.result);

      if (location >= 1 && location <= 2) {
        hitLocation = CONFIG.coverageTypes.head;
      } else if (location >= 3 && location <= 5) {
        hitLocation = CONFIG.coverageTypes.rarm;
      } else if (location >= 6 && location <= 8) {
        hitLocation = CONFIG.coverageTypes.larm;
      } else if (location >= 9 && location <= 14) {
        hitLocation = CONFIG.coverageTypes.torso;
      } else if (location >= 15 && location <= 17) {
        hitLocation = CONFIG.coverageTypes.rleg;
      } else {
        hitLocation = CONFIG.coverageTypes.lleg;
      }
    }

    rollData.reroll = reroll;

    const rollResult = {
      damage,
      damageType,
      effects,
      hitLocation,
      rolls,
    };

    ConanChat.renderRollCard(rollResult, rollData, cardData, 'damage');
  }

  static async generateDamageRoll(rollData, cardData, actorData) {
    const generatorErr = {
      reload:
        'Conan 2D20 | Error in Damage Roll, you must enter a number of reloads to spend',
      resource:
        'Conan 2D20 | Error in Damage Roll, you must select a Reload to spend',
    };

    if (rollData.reloadItem !== '' && rollData.reloadModifier < 1) {
      throw generatorErr.reload;
    } else if (rollData.reloadModifier > 0 && rollData.reloadItem === '') {
      throw generatorErr.resource;
    }

    const weapon = rollData.extra.weapon;
    rollData.baseDamage = Number(weapon.system.damage.dice || 1);
    rollData.damageType = weapon.system.damage.type;

    const qualities = weapon.system.qualities?.value || [];
    rollData.improvised = qualities.find(
      quality => quality.type === 'improvised'
    )
      ? true
      : false;

    const attackType = rollData.attackType;
    rollData.attributeBonus = rollData.modifiers.attackBonuses[attackType];

    const damageType = CONFIG.damageTypes[rollData.damageType];

    const {attackerType} = rollData;

    let diceQty =
      rollData.baseDamage +
      rollData.attributeBonus +
      rollData.talentModifier +
      rollData.otherModifier;

    try {
      if (rollData.momentumModifier > 0) {
        if (attackerType === 'npc') {
          Conan2d20Actor.spendDoom(
            actorData,
            Number(rollData.momentumModifier)
          );
        } else {
          Conan2d20Actor.spendMomentum(actorData, rollData.momentumModifier);
        }
      }

      if (rollData.reloadModifier > 0) {
        Conan2d20Actor.spendReload(
          actorData,
          rollData.reloadModifier,
          rollData.reloadItem
        );
        diceQty += rollData.reloadModifier;
      }

      await this.calculateDamageRoll(diceQty, damageType, cardData, rollData);
    } catch (e) {
      console.log(e);
      ui.notifications.error(e);
    }
  }

  static async generateSkillRoll(baseDice = 2, rollData, cardData, actorData) {
    // TODO: Wire in momentum expenditure check
    const generatorErr = {
      res_count:
        'Conan 2D20 | Error in Skill Check, you must enter a number of resources to spend',
      bonus_count: 'Selection would be greater than 3 Bonus Dice. Not allowed.',
      assist_mods: 'Assist rolls cannot use momentum or Doom.',
    };

    if (rollData.diceModifierType !== '' && rollData.diceModifier === 0) {
      throw generatorErr.res_count;
    }

    // Make sure we're not being requested to roll too many dice including
    // any
    const totalBonusDice =
      rollData.diceModifierDoom +
      rollData.diceModifierMomentum +
      rollData.successModifier;

    if (baseDice + totalBonusDice > 5) {
      throw generatorErr.bonus_count;
    } else if (totalBonusDice > 3) {
      throw generatorErr.bonus_count;
    }

    let trained = rollData.skill.trained || false;

    let diceQty;
    let doomSpend;
    let fortuneConv;

    if (actorData.type === 'npc') {
      trained = rollData.skill.value > 0;

      const expertise =
        actorData.system.attributes[rollData.npcAttributes].value;

      const tn = rollData.skill.value + expertise;

      diceQty = baseDice;

      if (rollData.successModifier > 0) {
        doomSpend = rollData.diceModifierDoom;
        fortuneConv = rollData.successModifier * 3;
      } else {
        doomSpend = rollData.diceModifierDoom;
      }

      if (actorData.system.type === 'minion') {
        if (actorData.system.isMob) {
          diceQty = actorData.system.mobCount;
        } else {
          diceQty = 1;
        }
      } else {
        if (actorData.system.isMob) {
          // A toughened or nemesis in a mob is called a squad
          //
          // TODO Currently we only support squads with the same
          // attributes/skills as the main NPC which is is better than nothing,
          // but we could really do with allowing people to add a sub-NPC to use
          // as Squad members
          diceQty = actorData.system.mobCount + 1;
        } else {
          diceQty = 2;
        }
      }

      if (doomSpend > 0 || fortuneConv > 0) {
        if (rollData.successModifier > 0) {
          Conan2d20Actor.spendDoom(actorData, Number(doomSpend + fortuneConv));
        } else {
          Conan2d20Actor.spendDoom(actorData, Number(doomSpend));
        }
        diceQty += doomSpend;
        await this.calculateSkillRoll(
          diceQty,
          tn,
          rollData.skill.value,
          trained,
          rollData.difficulty,
          rollData.successModifier,
          cardData,
          undefined,
          rollData.momentumBonusModifier
        );
      } else {
        await this.calculateSkillRoll(
          diceQty,
          tn,
          rollData.skill.value,
          trained,
          rollData.difficulty,
          rollData.successModifier,
          cardData,
          undefined,
          rollData.momentumBonusModifier
        );
      }
    } else {
      // Character roll here
      diceQty = baseDice;

      if (rollData.diceModifierAssist) {
        if (
          rollData.diceModifierMomentum > 0 ||
          rollData.diceModifierDoom > 0
        ) {
          throw generatorErr.assist_mods;
        }
        diceQty = 1;
      }

      let tn;
      let focus;
      if (rollData.attrRoll) {
        tn = rollData.skill.value;
        focus = 0;
      } else {
        tn = rollData.skill.tn.value;
        focus = rollData.skill.focus.value;
      }
      if (rollData.rollBonusModifier > 0) {
        diceQty += rollData.rollBonusModifier;
      }
      if (rollData.diceModifierMomentum + rollData.diceModifierDoom > 0) {
        if (rollData.diceModifierMomentum > 0) {
          Conan2d20Actor.spendMomentum(
            actorData,
            rollData.diceModifierMomentum
          );

          diceQty += rollData.diceModifierMomentum;
        }
        if (rollData.diceModifierDoom > 0) {
          Conan2d20Actor.payDoom(actorData, rollData.diceModifierDoom);
          diceQty += rollData.diceModifierDoom;
        }

        if (rollData.successModifier > 0) {
          Conan2d20Actor.spendFortune(actorData, rollData.successModifier);
          await this.showFortuneSpendDialog(
            diceQty,
            tn,
            focus,
            trained,
            rollData.difficulty,
            rollData.successModifier,
            cardData
          );
        } else {
          await this.calculateSkillRoll(
            diceQty,
            tn,
            focus,
            trained,
            rollData.difficulty,
            rollData.successModifier,
            cardData,
            undefined,
            rollData.momentumBonusModifier
          );
        }
      } else if (rollData.successModifier > 0) {
        Conan2d20Actor.spendFortune(actorData, rollData.successModifier);
        await this.showFortuneSpendDialog(
          diceQty,
          tn,
          focus,
          trained,
          rollData.difficulty,
          rollData.successModifier,
          cardData
        );
      } else {
        await this.calculateSkillRoll(
          diceQty,
          tn,
          focus,
          trained,
          rollData.difficulty,
          rollData.successModifier,
          cardData,
          undefined,
          rollData.momentumBonusModifier
        );
      }
    }
  }

  static async showFortuneSpendDialog(
    diceQty,
    tn,
    focus = 0,
    trained = false,
    difficulty = 0,
    autoSuccess = 0,
    cardData
  ) {
    let dialogData;
    const template =
      'systems/conan2d20/templates/apps/fortune-roll-dialogue.html';
    return renderTemplate(template, dialogData).then(html => {
      new Dialog({
        content: html,
        title: game.i18n.localize('CONAN.rollRemainingLabel'),
        buttons: {
          yes: {
            label: game.i18n.localize('CONAN.rollYesLabel'),
            callback: () =>
              this.calculateSkillRoll(
                diceQty,
                tn,
                focus,
                trained,
                difficulty,
                autoSuccess,
                cardData,
                undefined
              ),
          },
          no: {
            label: game.i18n.localize('CONAN.rollNoLabel'),
            callback: () =>
              this.calculateSkillRoll(
                0,
                tn,
                focus,
                trained,
                difficulty,
                autoSuccess,
                cardData,
                undefined
              ),
          },
        },
        default: 'yes',
      }).render(true);
    });
  }

  static async showDamageRollDialog({
    dialogData,
    rollData,
    cardData,
    actorData,
  }) {
    return renderTemplate(
      'systems/conan2d20/templates/apps/damage-roll-dialogue.html',
      dialogData
    ).then(html => {
      new Dialog(
        {
          content: html,
          title: dialogData.title,
          buttons: {
            roll: {
              label: game.i18n.localize('CONAN.rollDamageLabel'),
              callback: async template => {
                rollData.attackType = (
                  template.find('[name="attackType"]').val() || ''
                ).toLowerCase();

                rollData.momentumModifier = Number(
                  template.find('[name="momentumModifier"]').val() || 0
                );

                rollData.reloadModifier = Number(
                  template.find('[name="reloadModifier"]').val() || 0
                );

                rollData.reloadItem =
                  template.find('[name="reloadItem"]').val() || '';

                rollData.talentModifier = Number(
                  template.find('[name="talentModifier"]').val() || 0
                );

                rollData.otherModifier = Number(
                  template.find('[name="otherModifier"]').val() || 0
                );

                rollData.attackerType = dialogData.modifiers.attacker;

                try {
                  await Conan2d20Dice.generateDamageRoll(
                    rollData,
                    cardData,
                    actorData
                  );
                } catch (e) {
                  console.log(e);
                  ui.notifications.error(e);
                }
              },
            },
          },
        },
        {classes: ['roll-dialog']}
      ).render(true);
    });
  }

  static async showSkillRollDialog({
    dialogData,
    rollData,
    cardData,
    actorData,
  }) {
    return renderTemplate(
      'systems/conan2d20/templates/apps/skill-roll-dialogue.html',
      dialogData
    ).then(html => {
      new Dialog(
        {
          content: html,
          title: dialogData.title,
          buttons: {
            roll: {
              label: game.i18n.localize('CONAN.rollSkillLabel'),
              callback: async template => {
                rollData.difficulty = Number(
                  template.find('[name="difficulty"]').val() || 0
                );
                rollData.diceModifierAssist = Boolean(
                  template.find('[name="diceModifierAssist"]').is(':checked') ||
                    0
                );
                rollData.diceModifierMomentum = Number(
                  template.find('[name="diceModifierMomentum"]').val() || 0
                );
                rollData.diceModifierDoom = Number(
                  template.find('[name="diceModifierDoom"]').val() || 0
                );
                rollData.successModifier = Number(
                  template.find('[name="successModifier"]').val() || 0
                );
                rollData.rollBonusModifier = Number(
                  template.find('[name="rollBonusModifier"]').val() || 0
                );
                rollData.momentumBonusModifier = Number(
                  template.find('[name="momentumBonusModifier"]').val() || 0
                );
                rollData.npcAttributes =
                  template.find('[name="npcAttributes"]').val() || '';
                let baseDice = 2;

                if (dialogData.modifiers.actorType === 'npc') {
                  if (actorData.system.type === 'minion') {
                    baseDice = 1;
                  }
                }

                try {
                  await Conan2d20Dice.generateSkillRoll(
                    baseDice,
                    rollData,
                    cardData,
                    actorData
                  );
                } catch (e) {
                  console.log(e);
                  ui.notifications.error(e);
                }
              },
            },
          },
        },
        {classes: ['roll-dialog']}
      ).render(true);
    });
  }

  /**
   * Activate event listeners using the chat log html.
   * @param html {HTML}  Chat log html
   */
  static async chatListeners(html) {
    // Custom entity clicks
    html.on('click', '.reroll', ev => {
      const button = $(ev.currentTarget);
      const messageId = button.parents('.message').attr('data-message-id');
      const message = game.messages.get(messageId);

      const rolls = [];
      $(message.content)
        .children('.roll')
        .each(function () {
          rolls.push($(this).text().trim());
        });
    });

    html.on('click', '.roll-list-entry', ev => {
      const target = $(ev.currentTarget);
      const messageId = target.parents('.message').attr('data-message-id');
      const message = game.messages.get(messageId);

      if (message.isAuthor || game.user.isGM) {
        if (message.flags.data.rollData.reroll === false) {
          target.toggleClass('selected');

          const newHtml = target.parents().children('.message-content').html();

          message.update({content: newHtml});
        }
      }
    });

    html.on('click', '.chat-bank-momentum', ev => {
      const target = $(ev.currentTarget);
      const messageId = target.parents('.message').attr('data-message-id');
      const message = game.messages.get(messageId);

      if (message.isAuthor || game.user.isGM) {
        const actor = game.actors.get(message.speaker.actor);
        if (actor.system.momentum <= 0) {
          ui.notifications.warn(game.i18n.localize('CONAN.noUnbankedMomentum'));
        } else {
          new MomentumBanker(actor).render(true);
        }
      }
    });

    html.on('click', '.chat-execute-attack', ev => {
      ev.preventDefault();

      const target = $(ev.currentTarget);

      const messageId = target.parents('.message').attr('data-message-id');
      const message = game.messages.get(messageId);

      const tokenId = message.flags.conan2d20.tokenId;

      // If the tokenId is set on the message flags, then we need to use the
      // synthetic Token Actor rather than the base Actor, this will ensure any
      // changes to the synthetic Token Actor, such as boosted stats and/or
      // a change the Mob numbers is carried across correctly for the attack
      // roll.
      //
      let actor;
      if (tokenId) {
        actor = game.actors.tokens[tokenId];
      } else {
        actor = game.actors.get(message.speaker.actor);
      }

      const weapon = actor.getEmbeddedDocument(
        'Item',
        message.flags.conan2d20.itemId
      );

      const weaponSkill = weapon.skillToUse(actor.type);

      const {dialogData, cardData, rollData} = actor.setupSkill(weaponSkill);

      Conan2d20Dice.showSkillRollDialog({
        dialogData,
        cardData,
        rollData,
        actorData: actor,
      });
    });

    html.on('click', '.chat-execute-damage', ev => {
      ev.preventDefault();

      const target = $(ev.currentTarget);

      const messageId = target.parents('.message').attr('data-message-id');
      const message = game.messages.get(messageId);

      const tokenId = message.flags.conan2d20.tokenId;

      // If the tokenId is set on the message flags, then we need to use the
      // synthetic Token Actor rather than the base Actor, this will ensure any
      // changes to the synthetic Token Actor, such as boosted stats and/or
      // a change the Mob numbers is carried across correctly for the attack
      // roll.
      //
      let actor;
      if (tokenId) {
        actor = game.actors.tokens[tokenId];
      } else {
        actor = game.actors.get(message.speaker.actor);
      }

      const weapon = actor.getEmbeddedDocument(
        'Item',
        message.flags.conan2d20.itemId
      );

      const reloadIds = actor.items
        .filter(i => i.system.kitType === 'reload')
        .map(i => ({id: i.id, name: i.name, uses: i.system.uses.value} || []));

      const {dialogData, cardData, rollData} = actor.setupWeapon(
        weapon,
        reloadIds
      );

      Conan2d20Dice.showDamageRollDialog({
        dialogData,
        cardData,
        rollData,
        actorData: actor,
      });
    });

    html.on('click', '.chat-execute-soak', ev => {
      ev.preventDefault();

      const target = $(ev.currentTarget);

      const messageId = target.parents('.message').attr('data-message-id');
      const message = game.messages.get(messageId);

      const tokenId = message.flags.conan2d20.tokenId;

      // If the tokenId is set on the message flags, then we need to use the
      // synthetic Token Actor rather than the base Actor.
      //
      let actor;
      if (tokenId) {
        actor = game.actors.tokens[tokenId];
      } else {
        actor = game.actors.get(message.speaker.actor);
      }

      const item = actor.getEmbeddedDocument(
        'Item',
        message.flags.conan2d20.itemId
      );

      const soakDice = item.getSoak();

      const cardData = actor.setupSoak(item);
      const rollData = {soakDice};

      Conan2d20Dice.calculateSoakRoll(soakDice, cardData, rollData);
    });
  }

  /**
   * Add support for the Dice So Nice module
   * @param {Object} roll
   * @param {String} rollMode
   */
  static async showDiceSoNice(roll, rollMode) {
    if (
      game.modules.get('dice-so-nice') &&
      game.modules.get('dice-so-nice').active
    ) {
      let whisper = null;
      let blind = false;
      switch (rollMode) {
        case 'blindroll': {
          blind = true;
          break;
        }
        case 'gmroll': {
          const gmList = game.users.filter(user => user.isGM);
          const gmIDList = [];
          gmList.forEach(gm => gmIDList.push(gm.data.id));
          whisper = gmIDList;
          break;
        }
        case 'roll': {
          const userList = game.users.filter(user => user.active);
          const userIDList = [];
          userList.forEach(user => userIDList.push(user.data._id));
          whisper = userIDList;
          break;
        }
        default: {
          break;
        }
      }
      await game.dice3d.showForRoll(roll, game.user, true, whisper, blind);
    }
  }

  /**
   * Merge new rolls and the preserved old rolls submitted to a reroll request
   */
  static async _mergeRerolls(newRolls, oldRolls) {
    const mergedRolls = [];

    let x = 0;
    for (let i = 0; i < oldRolls.length; i += 1) {
      const mergeRoll = oldRolls[i];
      if (mergeRoll.index === x) {
        mergedRolls.push(mergeRoll);
        x++;
        continue;
      }

      while (x < mergeRoll.index) {
        mergedRolls.push(newRolls.shift());
        x++;
      }

      mergedRolls.push(mergeRoll);
      x++;
    }

    // Now make sure we merge any left over rolls
    return [...mergedRolls, ...newRolls];
  }
}
