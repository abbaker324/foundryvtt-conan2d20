import Counter from './counter';
import SkillRoller from '../apps/skill-roller';

export async function createItemMacro(dropData, slot) {
  const itemData = await Item.implementation.fromDropData(dropData);

  if (!itemData) {
    return ui.notifications.warn(
      game.i18n.localize('CONAN.Macro.Warn.CreateItemRequiresOwnership')
    );
  }

  const macroData = {
    command: `game.conan2d20.macros.postItem("${dropData.uuid}")`,
    flags: {'conan2d20.itemMacro': true},
    img: itemData.img,
    name: itemData.name,
    scope: 'actor',
    type: 'script',
  };

  // Assign the macro to the hotbar
  const macro =
    game.macros.find(
      m =>
        m.name === macroData.name &&
        m.command === macroData.command &&
        m.author.isSelf
    ) || (await Macro.create(macroData));

  game.user.assignHotbarMacro(macro, slot);
}

export async function initGame() {
  const user = game.user;

  if (!user.isGM) {
    return ui.notifications.error(
      game.i18n.format('CONAN.Macro.Error.GameMasterRoleRequired', {
        macro: 'Initialize Game',
      })
    );
  } else {
    try {
      const players = game.users.players;

      let startingDoom = 0;

      for (const player of players) {
        const actor = player.character;

        // Reset current Vigor and Resolve to max.
        actor.update({'system.health.mental.value': actor.getMaxResolve()});
        actor.update({'system.health.physical.value': actor.getMaxVigor()});

        // Reset Fortune
        const startingFortune = actor.system.resources.fortune.max;
        actor.update({'system.resources.fortune.value': startingFortune});

        startingDoom += startingFortune;

        // Also purge any leftover personal momentum
        actor.update({'system.momentum': 0});
      }

      // Momentum is reset to zero
      Counter.setCounter(0, 'momentum');

      // Set Doom to starting value (sum of all players' starting Fortune)
      Counter.setCounter(startingDoom, 'doom');

      return ui.notifications.info(
        game.i18n.format('CONAN.Macro.Success', {
          macro: 'Initialize Game',
        })
      );
    } catch (e) {
      return ui.notifications.error(
        game.i18n.format('CONAN.Macro.Error.CaughtError', {
          macro: 'Initialize Game',
          error: e,
        })
      );
    }
  }
}

export async function newScene() {
  const user = game.user;

  if (!user.isGM) {
    return ui.notifications.error(
      game.i18n.format('CONAN.Macro.Error.GameMasterRoleRequired', {
        macro: 'New Scene',
      })
    );
  } else {
    try {
      const players = game.users.players;

      for (const player of players) {
        const actor = player.character;

        // Reset current Vigor and Resolve to max.
        actor.update({'system.health.mental.value': actor.getMaxResolve()});
        actor.update({'system.health.physical.value': actor.getMaxVigor()});

        // Also purge any leftover personal momentum
        actor.update({'system.momentum': 0});
      }

      // Now reduce the momentum pool by one
      Counter.changeCounter(-1, 'momentum');

      return ui.notifications.info(
        game.i18n.format('CONAN.Macro.Success', {
          macro: 'New Scene',
        })
      );
    } catch (e) {
      return ui.notifications.error(
        game.i18n.format('CONAN.Macro.Error.CaughtError', {
          macro: 'New Scene',
          error: e,
        })
      );
    }
  }
}

export async function postItem(itemUuid) {
  // This is very basic for now, we just post any item to chat
  const item = await fromUuid(itemUuid);
  item.postItem();
}

export async function skillRoll() {
  new SkillRoller().render(true);
}
