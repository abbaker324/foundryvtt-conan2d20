/* eslint-disable no-unused-vars, no-shadow */
import {CONFIG} from '../../scripts/config';
import Conan2d20Dice from './rolls';
import Conan2d20Actor from '../actor/actor';

class ConanChat {
  static renderRollCard(rollResult, rollData, cardData, type) {
    rollResult.title = cardData.title;

    if (type === 'skill') {
      rollResult.difficultyString =
        CONFIG.rollDifficultyLevels[rollResult.difficulty];

      rollResult.focus = rollData.focus;
      rollResult.result = CONFIG.rollResults[rollResult.result];
      rollResult.tn = rollData.tn;
      rollResult.trained = rollData.trained;
    } else if (type === 'damage') {
      rollResult.improvised = rollData.improvised;
      rollResult.baseDamage = rollData.baseDamage;
      rollResult.totalDamageDice = rollResult.rolls.length;
      rollResult.attributeBonus = rollData.attributeBonus;
      rollResult.momentumModifier = rollData.momentumModifier;
      rollResult.qualities = rollData.extra.weapon.system.qualities.value;
      rollResult.reloadModifier = rollData.reloadModifier;
      rollResult.talentModifier = rollData.talentModifier;
    }

    cardData['flags.data'] = {
      resultData: rollResult,
      title: cardData.title,
      template: cardData.template,
      type,
      rollData,
    };

    renderTemplate(cardData.template, rollResult).then(html => {
      ChatMessage.create({
        title: cardData.title,
        content: html,
        'flags.data': cardData['flags.data'],
        speaker: cardData.speaker,
      });
    });
  }
}
export default ConanChat;

// Activate chat listeners defined in rolls
Hooks.on('renderChatLog', (log, html, data) => {
  Conan2d20Dice.chatListeners(html);
});

Hooks.on('getChatLogEntryContext', (html, options) => {
  const canApply = li => li.find('.skill-roll-card').length && game.user.isGM;

  const canReroll = function (li) {
    let result = false;
    const message = game.messages.get(li.attr('data-message-id'));

    if (message.isAuthor || game.user.isGM) {
      const card = li.find('.roll-card');
      if (card.length && message.flags.data.rollData.reroll === false) {
        result = true;
      }
    }
    return result;
  };

  options.push({
    name: game.i18n.localize('CONAN.CHATOPT.triggerReroll'),
    icon: '<i class="fas fa-dice"></i>',
    condition: canReroll,
    callback: li => {
      const message = game.messages.get(li.attr('data-message-id'));
      const actor = game.actors.get(message.speaker.actor);
      try {
        actor.triggerReroll(message, message.flags.data.type);
      } catch (e) {
        console.log(e);
        ui.notifications.error(e);
      }
    },
  });
});
